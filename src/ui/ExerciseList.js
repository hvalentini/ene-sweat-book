import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ListView,
  Text,
  TouchableWithoutFeedback
} from 'react-native';
import Button from 'apsl-react-native-button';
import { TopBar } from './TopBar.js';
import { SearchBar } from './SearchBar.js';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { fuzzySearch } from '../services/fuzzySearch.js';

export class ExerciseList extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      matchingExercises: ds.cloneWithRows([]),
    };
    this.handleSearch = this.handleSearch.bind(this);
  }

  handleSearch(searchTerm) {
    const filterExercises = !searchTerm || searchTerm.length < 3
                              ? []
                              : fuzzySearch(searchTerm, this.props.exercises, 'name');
    const matchingExercises = this.state.matchingExercises.cloneWithRows(filterExercises);
    this.setState({ matchingExercises });
  }

  handlePress(exercise) {
    this.props.addExercise(exercise);
    this.props.closeModal();
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#efefef' }}>
        <TopBar style={styles.topbar}>
          <View style={{ flex: 0.1 }}>
            <Button
              onPress={this.props.closeModal}
              textStyle={styles.close}
              style={styles.closeButton}
            >
              <Icon name="arrow-back" size={35} color="#fff" />
            </Button>
          </View>
          <SearchBar
            containerStyle={styles.searchBar}
            onTextChange={this.handleSearch}
          />
        </TopBar>
        <ListView
          dataSource={this.state.matchingExercises}
          renderRow={(exercise) => (
            <TouchableWithoutFeedback
              onPress={() => this.handlePress(exercise)}
            >
              <View style={styles.row}>
                <Text style={styles.rowName}>
                  {exercise.name}
                </Text>
              </View>
            </TouchableWithoutFeedback>
          )}
          renderFooter={() => (
            <View style={styles.row}>
              <Text style={{ fontSize: 12 }}>
                ®Helton
              </Text>
            </View>
          )}
        />
      </View>
    );
  }
}

ExerciseList.propTypes = {
  closeModal: React.PropTypes.func,
  exercises: React.PropTypes.array,
  addExercise: React.PropTypes.func,
};

const styles = StyleSheet.create({
  searchBar: {
    backgroundColor: 'white',
    flex: 0.9,
    borderColor: 'grey',
    borderWidth: 1,
    height: 35,
    padding: 5,
    justifyContent: 'center',
    borderRadius: 2
  },
  topbar: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  input: {
    color: 'black',
    fontSize: 20
  },
  close: {
    fontSize: 34
  },
  closeButton: {
    borderWidth: 0
  },
  row: {
    borderWidth: 1,
    borderColor: 'grey'
  },
  rowName: {
    fontSize: 30
  }
});
