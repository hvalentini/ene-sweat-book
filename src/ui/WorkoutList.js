import React, { Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ListView
} from 'react-native';
import Button from 'apsl-react-native-button';
import Icon from 'react-native-vector-icons/MaterialIcons';

const { width } = Dimensions.get('window');

export class WorkoutList extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      currentWorkout: ds.cloneWithRows([props.currentWorkout]),
    };
  }
  render() {
    const workoutList = this.state.currentWorkout.cloneWithRows(this.props.currentWorkout);
    return (
      <View>
        <ListView
          style={styles.WorkoutList}
          dataSource={workoutList}
          renderRow={exercise => (
            <View style={styles.workout}>
              <Text style={styles.workoutText}>{exercise.name}</Text>
            </View>
          )}
          renderFooter={() => (
            <View style={styles.addSomeExecercises}>
              <Text style={styles.bigText}>Adicionar exercícios</Text>
              <Button
                style={styles.plusButton}
                textStyle={styles.plus}
                onPress={() => this.props.setModalVisibility(true)}
              ><Icon name="note-add" size={50} /></Button>
            </View>
          )}
        />
      </View>
    )
  }
}
WorkoutList.propTypes = {
  setModalVisibility: React.PropTypes.func,
  currentWorkout: React.PropTypes.array,
};

const styles = StyleSheet.create({
  addSomeExecercises: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10
  },
  bigText: {
    fontSize: 24,
    color: 'blue'
  },
  plusButton: {
    borderStyle: null,
    borderWidth: 0
  },
  plus: {
    color: 'black',
    fontSize: 60
  },
  workoutList: {
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    width: width * 0.80
  },
  workout: {
    borderBottomColor: 'rgba(255, 255, 255, 0.1)',
    borderBottomWidth: 1,
    padding: 10
  },
  workoutText: {
    color: 'rgba(0,0,0,.87)',
    fontSize: 24
  }
});
