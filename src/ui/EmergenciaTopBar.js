import React from 'react';
import TopBar from './TopBar.js';

export const EmergenciaTopBar = (props) => (
  <TopBar style={{ backgroundColor: '#f00' }}>
    {props.children}
  </TopBar>
);

EmergenciaTopBar.propTypes = {
  children: React.PropTypes.node,
};
