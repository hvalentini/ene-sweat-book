import React from 'react';
import { Modal } from 'react-native';
import { ExerciseList } from './ExerciseList.js';

export const ExerciseModal = (props) => (
  <Modal
    style={{ flex: 1 }}
    animationType={"slide"}
    visible={props.visible}
    onRequestClose={props.closeModal}
  >
    <ExerciseList {...props} />
  </Modal>
);


ExerciseModal.propTypes = {
  visible: React.PropTypes.bool,
  closeModal: React.PropTypes.func,
};
