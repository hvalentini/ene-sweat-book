import { createAction } from 'redux-actions';
import * as types from './types';
import * as api from '../services/api.js';

// export const setExerciseModalVisibility = (visible) => (
//   createAction(types.SET_EXERCISE_VISIBILITY)(visible);
// );

export const setExerciseModalVisibility = (visible) => {
  return dispatch => {
    // dispatch(createAction(types.SET_EXERCISE_VISIBILITY)());
    setTimeout(() => {
      // Yay! Can invoke sync or async actions with `dispatch`
      dispatch(createAction(types.SET_EXERCISE_VISIBILITY_COMPLETE)(visible));
    }, 0);
  };
};

export const addExercise = (exercise) => (
  dispatch => {
    api
      .post('current_workout', exercise)
      .then(exerciseJSON => dispatch(createAction(types.ADD_EXERCISE)(exerciseJSON))
    );
  }
);

export const fetchWorkout = () => (
  dispatch => {
    api
      .get('current_workout')
      .then(workout => dispatch(createAction(types.FETCH_CURRENT_WORKOUT_COMPLETE)(workout))
    );
  }
);
