import React from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Button from 'apsl-react-native-button';

export const Welcome = (props) => (
  <LinearGradient
    colors={['#007E71', '#00B7A1']}
    style={styles.container}
  >
    <View style={styles.titleContianer}>
      <Text style={styles.title}>
        Sweat Book Ene
      </Text>
    </View>
    <View style={styles.lastWorkoutContainer}>
      <Text style={styles.lastWorkoutTitle}>
        Seu último treino
      </Text>
      <Text style={styles.lastWorkoutTitle}>
        Segunda, 07 de novembro
      </Text>
    </View>
    <View style={{ padding: 30 }}>
      <Button
        style={styles.button}
        textStyle={styles.buttonText}
        onPress={props.startWorkout}
        children={'INICIAR TREINO'}
      />
    </View>
  </LinearGradient>
);

Welcome.propTypes = {
  startWorkout: React.PropTypes.func,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  titleContianer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 0.2
  },
  title: {
    fontSize: 48,
    fontWeight: '500',
    color: 'rgba(255,255,255,.87)',
    textAlign: 'center'
  },
  lastWorkoutContainer: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lastWorkoutTitle: {
    fontSize: 30,
    fontWeight: '100',
    color: 'rgba(255,255,255,.87)',
  },
  button: {
    borderColor: '#EADCDC',
    padding: 20,
    borderRadius: 10
  },
  buttonText: {
    color: 'rgba(255,255,255,.87)',
    fontSize: 28
  }
});
