import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import { TopBar } from '../ui/TopBar.js';
import { connect } from 'react-redux';
import {
  setExerciseModalVisibility,
  addExercise,
  fetchWorkout
} from '../actions/actions.js';
import { WorkoutList } from '../ui/WorkoutList.js';
import { ExerciseModal } from '../ui/ExerciseModal.js';

class Container extends Component {
  componentDidMount() {
    this.props.fetchWorkout();
  }

  render() {
    return (
      <View>
        <TopBar style={styles.topBar}>
          <Text style={styles.text}>Treino atual</Text>
          {this.props.isLoading && <Text style={{ color: '#fff' }}> Carregando...</Text>}
        </TopBar>
        <View style={styles.currentWorkout}>
          <WorkoutList
            currentWorkout={this.props.currentWorkout}
            setModalVisibility={this.props.setExerciseModalVisibility}
          />
        </View>
        <ExerciseModal
          addExercise={this.props.addExercise}
          exercises={this.props.exercises}
          visible={this.props.exerciseModal}
          closeModal={() => { this.props.setExerciseModalVisibility(false); }}
        />
      </View>
    );
  }
}

Container.propTypes = {
  exerciseModal: React.PropTypes.bool,
  setExerciseModalVisibility: React.PropTypes.func,
  fetchWorkout: React.PropTypes.func,
  addExercise: React.PropTypes.func,
  exercises: React.PropTypes.array,
  isLoading: React.PropTypes.bool,
  currentWorkout: React.PropTypes.array,
};

const mapStateToProps = (state) => ({
  exerciseModal: state.ui.exerciseModal,
  isLoading: state.ui.isLoading,
  exercises: state.exercises,
  currentWorkout: state.currentWorkout
});

const mapActionToProps = (dispatch) => ({
  fetchWorkout() {
    dispatch(fetchWorkout());
  },

  setExerciseModalVisibility(visible) {
    dispatch(setExerciseModalVisibility(visible));
  },

  addExercise(exercise) {
    dispatch(addExercise(exercise));
  }
});

export const CurrentWorkout = connect(mapStateToProps, mapActionToProps)(Container);

const styles = StyleSheet.create({
  text: {
    color: 'rgba(255,255,255,.87)',
    fontSize: 24
  },
  container: {
    flex: 1
  },
  topbar: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderBottomColor: 'white',
    borderBottomWidth: 2
  },
  currentWorkout: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
