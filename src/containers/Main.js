import React from 'react';
import {
  Text
} from 'react-native';
import TabView from 'react-native-scrollable-tab-view';
import LinearGradient from 'react-native-linear-gradient';
import { CurrentWorkout } from './CurrentWorkout.js'

export const Main = () => (
  <LinearGradient
    colors={['#F7F7F7', '#D7D7D7']}
    style={styles.container}
  >
    <TabView tabBarPosition="overlayBottom" tabBarTextStyle={styles.tabText}>
      <CurrentWorkout tabLabel="+" />
      <Text tabLabel="Historico">Aba 2</Text>
    </TabView>
  </LinearGradient>
);


const styles = {
  container: {
    flex: 1
  },
  tabText: {
    fontSize: 30
  }
};
