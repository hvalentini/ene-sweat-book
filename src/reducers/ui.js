import * as types from '../actions/types';

export const ui = (state = {
  exerciseModal: false,
  isLoading: false,
}, { type, payload }) => {

  switch (type) {
    case types.SET_EXERCISE_VISIBILITY:
      return {
        ...state,
        isLoading: true
      }
    case types.SET_EXERCISE_VISIBILITY_COMPLETE:
      return {
        ...state,
        exerciseModal: payload,
        isLoading: false
      };
    default:
      return state;
  }
};
