import React, { Component } from 'react';
import { Welcome } from './containers/Welcome.js';
import { Main } from './containers/Main.js';
import { Provider } from 'react-redux';
import { store } from './store';

export class Root extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startWorkout: false
    };
    this.startWorkout = this.startWorkout.bind(this);
  }
  startWorkout() {
    this.setState({ startWorkout: true });
  }
  renderRoot(ComponentToRender) {
    return (
      <Provider store={store}>
        <ComponentToRender startWorkout={this.startWorkout} />
      </Provider>
    );
  }
  render() {
    const ComponentToRender = this.state.startWorkout
                                ? Main
                                : Welcome;
    return this.renderRoot(ComponentToRender);
  }
}
