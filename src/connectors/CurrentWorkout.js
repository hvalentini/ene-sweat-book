/****************************************************/
/* não está sendo utilizado - somente para exemplos */
/****************************************************/

import { connect } from 'react-redux';
import {
  setExerciseModalVisibility,
  addExercise
} from '../actions/actions.js';

const mapStateToProps = (state) => ({
  exerciseModal: state.ui.exerciseModal,
  exercises: state.exercises,
});

const mapActionToProps = (dispatch) => ({
  setExerciseModalVisibility(visible) {
    dispatch(setExerciseModalVisibility(visible));
  },
  addExercise(exercise) {
    dispatch(addExercise(exercise));
  }
});

export const CWConnector = connect(mapStateToProps, mapActionToProps);
